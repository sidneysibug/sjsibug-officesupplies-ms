<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => 'Printer'

        ]);

        DB::table('categories')->insert([
        	'name' => 'Paper',
        ]);

        DB::table('categories')->insert([
        	'name' => 'Laptop'
 
        ]);
    }
}
