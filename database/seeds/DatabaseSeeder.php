<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
         	RoleSeeder::class,
         	CategorySeeder::class,
            AvailabilitySeeder::class,
            UserSeeder::class,
            StatusSeeder::class,
         	UnitSeeder::class,
         	TransactionSeeder::class

         ]);
    }
}
