<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AvailabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('availabilities')->insert([
        	'name' => 'available'
        ]);

         DB::table('availabilities')->insert([
        	'name' => 'unavailable'
        ]);

         DB::table('availabilities')->insert([
        	'name' => 'under maintenance'
        ]);
    }
}
