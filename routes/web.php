<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/status/{id}', 'TransactionController@status');
Route::delete('/requestform', 'RequestFormController@clear')->name('requestform.clear');

Route::resources([
	'categories' => 'CategoryController',
	'units' => 'UnitController',
	'transactions' => 'TransactionController',
	'requestform' => 'RequestFormController'
	
]);
