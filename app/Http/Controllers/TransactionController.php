<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Unit;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        

      $transactions = Transaction::paginate(5);
      //returns an array
      //dd($transactions);

      $statuses = Status::all();
      $units = Unit::all();

      // if(request()->has('status')) {

      //   //$displays = Transaction::where('status', request('status'))->paginate(5);

      //   $displays = Transaction::where('status', request('status'))
      //       ->paginate(5)->appends('status', request('status'));


      // } else {

      // $displays = Transaction::paginate(5);

      // }

      // if(request()->has('status_id')){

      //   $transactions = $transactions->where('status_id', request('status_id'));
      // }

      //   $transactions = $transactions->paginate(5)->appends([
      //   'status_id' => request('status_id')
      // ]);




     // $posts = Transaction::paginate()

      return view('transactions.index', compact('transactions'))
        ->with('transactions', $transactions)
        ->with('statuses', $statuses);
        
        
         
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function rules()
    {

    }


    public function store(Request $request)
    {
        //validate dates
        $validatedData = $request->validate([
          'date_needed' => 'required',
          'date_return' => 'required|after_or_equal:date_needed',
          'reason' => 'required|string'

        ]);

        //dd($validatedData);

        //get the details needed 
        $transaction = new Transaction;
        //transaction_code
        $transaction->request_code = strtoupper(Str::random(10));

        //user_id
        $transaction->user_id = Auth::user()->id;
        $transaction->date_needed = $request->date_needed;
        $transaction->date_return = $request->date_return;

        //reason
        $transaction->reason = $request->reason;
        
        $transaction->save();
        //dd($transaction);



        $units = Unit::find(array_keys(session('requestform')));

        foreach ($units as $unit) {
          
          //product_id or unit_id
          $unit_id = $unit->id;

          $transaction_id = $transaction->id;

          //query to pivot table
          $transaction->units()->attach($unit->id,[
            'transaction_id' => $transaction_id,
            'unit_id' => $unit_id
          ]);
        }

        $transaction->save();
        //dd($transaction);

        session()->forget('requestform');

        return redirect( route('transactions.show', $transaction->id ));
          

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction) 
    {
        //
      
      $statuses = Status::all();
      return view('transactions.show')
        ->with('transaction', $transaction)
        ->with('statuses', $statuses)
        ->with('message', "Return date must be later than date needed");
        


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
        
          

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
      $transaction->status_id = $request->status_id;
      $transaction->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }


    public function status($stat_id)
    {

        $statuses = Status::all();
        $transactions = DB::table('transactions')
            ->join('statuses', 'transactions.status_id', '=', 'statuses.id')
            ->select('transactions.*', 'statuses.*')
            ->where(['statuses.id' => $stat_id])
            ->get();
        //dd($transactions);

        return view('statuses.statusestransactions', ['statuses' => $statuses, 'transactions' => $transactions]);
            
    }
}
