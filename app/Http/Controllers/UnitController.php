<?php
 
namespace App\Http\Controllers;

use App\Unit;
use App\Category;
use App\Status;
use App\Availability;
use Illuminate\Http\Request;
use Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $units = Unit::all()->sortBy('name');
        $availabilities = Availability::all();
        
        //dd($units);

        return view('units.index')
            ->with('units', $units)
            ->with('availabilities', $availabilities);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        // $unit_code = strtoupper(Str::random(10));
        $this->authorize('create', Unit::class);
        return view('units.create')
            ->with('categories', Category::all());
            // ->with('unit_code', $unit_code);
           
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->authorize('create',Unit::class);
        $categories = Category::all();

        //quantity- all units in a category
        // foreach ($categories as $category) {
        //     $category->quantity = DB::table('units')->where('category_id',$category->id)->count();
        //     $category->save();

        // }

        //stocks-all units in a category with status available/availability_id = 1
        // foreach ($categories as $category) {
        //   $category->stocks = DB::table('units')->where('category_id',$category->id)->where('units.availability_id',1)->count();
        //   $category->save();
        // }
        

        //$this->authorize('create', Unit::class);
        //dd($request);
        
        //dd($request);
        $validatedData = $request->validate([
           'image' => 'required',
           'particulars' => 'required|string',
           'uom' => 'required|string',
            'category_id' => 'required',
            'unit_code' => 'required'
            
        ]);

        //$unit_code = strtoupper(Str::random(10));
        //dd($unit_code);

        $path = $request->file('image')->store('public/units');

        $url = (Storage::url($path));

        $unit = new Unit($validatedData);
        //dd($unit);

        $cat_id = $unit->category_id;
        //dd($cat_id);

        //$unit->unit_code = $request->unit_code;
        //dd($unit->unit_code);

        $unit->unit_code = $cat_id . $request->unit_code;
        //dd($unit->unit_code);

        $unit->image = $url;
        //dd($unit->unit_code);

        $unit->save();
        //dd($unit);

        //View::share('unit_code', $unit->unit_code);

        // return redirect( route('units.index'))
        //     ->with('unit', $unit);
            //->with('message', "Unit {$unit->name} is added successfully");
            //->with('unit_code', $unit_code );

        return redirect(route('categories.show',$cat_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
        return view('units.show')
            ->with('unit', $unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        //
        $this->authorize('update', $unit);
        return view('units.edit')
            ->with('unit', $unit)
            ->with('categories', Category::all())
            ->with('availabilities', Availability::all());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        $this->authorize('update', $unit);
        $validatedData = $request->validate([
            'image' => 'image',
            'availability_id' => 'required'
        ]);

        $unit->update($validatedData);

        if($request->hasFile('image')){
            $path = $request->file('image')->store('public/units');
            $url = (Storage::url($path));
            $unit->image = $url;
        }

        $unit->save();
        //dd($unit);
      

         return redirect( route('units.index') );
          // ->with('message', "Unit {$unit->name} is updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        //
        $this->authorize('delete',$unit);
        $unit->delete();
        return redirect( route('units.index'))
            ->with('message', "Unit {$unit->name} is deleted successfully");

    }


}
