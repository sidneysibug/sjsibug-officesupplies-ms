<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    //
    use SoftDeletes;

    protected $fillable = ['image', 'particulars', 'uom', 'category_id', 'availability_id'];

    public function category(){

    	return $this->belongsTo('App\Category');

    }

    public function availability(){

    	return $this->belongsTo('App\Availability');

    }

    // public function transaction(){

    // 	return $this->belongsTo('App\RequestList');

    // }

   

    
}