<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
     public function status()
   {
   		return $this->belongsTo('App\Status');
   }


   public function user()
   {
   		return $this->belongsTo('App\User');
   }

   public function units()
   {
   		return $this->belongsToMany('App\Unit')
   			->withTimestamps();
   }

   // public function units(){

   //    return $this->hasMany('App\Unit');
   // }
}
