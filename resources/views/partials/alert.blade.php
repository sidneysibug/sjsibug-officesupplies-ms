<div class="row">
	<div class="col-12">
		<div class="alert alert-{{ Session::has('alert') ? Session::get('alert') : "success" }}">
			{{ Session::get('message') }}
		</div>
	</div>
</div>