@extends('layouts.app')

@section('content')
	<div class="container">
		
		<div class="row">
			<div class="col-12 my-4">
				@include('transactions.partials.header')
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<h4>View Single Request</h4>
			</div>
		</div>
		<hr>

		{{-- start of requestlist section --}}
		<div class="row">
			<div class="col-12 col-md-6">
				@include('transactions.partials.summary')
			</div>
		</div>
		@include('transactions.partials.units-table')
		{{-- end of requestlist section --}}
	</div>
@endsection