@extends('layouts.app')

@section('content')
	<div class="container">

		<div class="row">
			<div class="col-12 col-md-6 mt-4 mb-2">
				@include('transactions.partials.header')
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-8">
				<h4 class="">
					View all
				</h4>
			</div>
		</div>
		<hr>

		{{-- start of requests section --}}
		<div class="row">
			<div class="col-12 col-md-8">
				
				@include('transactions.partials.accordion')
				
			</div>
		</div>
		{{-- end of requests section --}}
	</div>

@endsection