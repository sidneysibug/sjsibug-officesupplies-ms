<div class="table-responsive">
	<?php $user = App\User::find($transaction->user_id)?>
	<?php $status = App\Status::find($transaction->status_id)?>
	<table class="table table-hover">
		{{-- transacation code --}}
		<tr>
			<td>Reference number:</td>
			<td>{{$transaction->request_code}}</td>
		</tr>
		{{-- transaction code end --}}
		
		{{-- Customer name start --}}
		<tr>
			<td>Customer</td>
			<td>{{ $user->name }}</td>
		</tr>
		{{-- Customer name end --}}

		{{-- Date start --}}
		<tr>
			<td>Date requested:</td>
			<td>{{ date("M d, Y", strtotime($transaction->created_at)) }}</td>
		</tr>
		{{-- Date end --}}


		{{-- Status start --}}
		<tr>
			<td>Status</td>
			<td>{{ $status->name}}</td>
			@can('isAdmin')
				<td>
					@include('transactions.partials.edit-status')
				</td>
			@endcan
		</tr>
		{{-- Status end --}}

		{{-- start of reason --}}
		<tr>
			<td>Reason for the request:</td>
			<td>{{ $transaction->reason}}</td>
		</tr>
		{{-- end of reason --}}

		{{-- borrow_date start --}}
		<tr>
			<td>Date needed:</td>
			<td>{{ date("M d, Y", strtotime($transaction->date_needed)) }}</td>
		</tr>
		{{-- borrow_date end --}}

		{{-- borrow_date start --}}
		<tr>
			<td>Date to return:</td>
			<td>{{ date("M d, Y", strtotime($transaction->date_return)) }}</td>
		</tr>
		{{-- borrow_date end --}}

		{{-- start of requested unit --}}
		<tfoot>
			<tr>
				<th>Requested Unit:</th>
			</tr>
			<tr>

				{{-- 
				@foreach($transaction->units as $unit)
=======
				{{<?php $units = App\Unit::findOrFail($transaction->unit->unit_id)?> --}}
				
				{{-- @foreach($transaction->units as $unit)
>>>>>>> feat-pagination
					
				<td>{{ $unit->unit_code}}</td>
				<td>{{ $unit->availability->name}}</td>

				@endforeach --}}
			</tr>
		</tfoot>
		{{-- end of requested unit --}}

	</table>
</div>
{{-- table end --}}