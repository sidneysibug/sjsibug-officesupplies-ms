@extends('layouts.app')

@section('content')
	<div class="container">

{{-- 		Filter: 
		<a href="{{ route('transactions.index', ['status_id' => request('status_id')]) }}">		Pending
		</a>
		<a href="{{ route('transactions.index', ['status_id' => request('status_id')]) }}">Approved</a>
		<a href="/?status_id=3">Declined</a>

		<a href="{{ route('transactions.index', ['status' => request('status')]) }}">Pending</a>
		<a href="/?status_id=2">Approved</a>
		<a href="/?status_id=3">Declined</a>

		<a href="{{ route('transactions.index', ['status_id' => request('status_id')]) }}">Ascending</a>
		

		<table>
			
		@foreach($displays as $transaction)
			<tr>
				<th>{{ $transaction->request_code}}</td>
				<th>{{ $transaction->status->name}}</td>
			</tr>
		@endforeach

		</table> --}}

{{-- 		<ul class="list-group">
			@if(count($statuses) > 0)
				@foreach($statuses->all() as $status)
					<li class="list-group-item">
						<a href="{{ url("status/{$status->id}") }}">{{ $status->name}}</a>
					</li>
				@endforeach
			@else 
				<p>No status found</p>
			@endif
		</ul>
 --}}
		

		<div class="row">
			<div class="col-12 col-md-6 mt-4 mb-2">
				@include('transactions.partials.header')
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-8">
				<h4 class="">
					View
				</h4>
			</div>
		</div>
		<hr>

		<div class="row">
			<div class="col-12 col-md-8">
				@include('transactions.partials.accordion')


				{{-- @foreach($transactions as $transaction)

					<?php $status = App\Status::find($transaction->status_id)?>

					{{ $transaction->request_code}}
					{{ $status->name}}
				@endforeach --}}
			</div>
		</div>
	</div>

@endsection