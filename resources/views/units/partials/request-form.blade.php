<form action="{{route('requestform.update', $unit->id) }}" method="post">	
	@csrf
	@method('PUT')
	
	<button 
		class="btn btn-sm btn-info w-100 mt-1"
		{{ $unit->availability_id !== 1 ? "disabled" : ""}}
	>
		Request
	</button>
</form>