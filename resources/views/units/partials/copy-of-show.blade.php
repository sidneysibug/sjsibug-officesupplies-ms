@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row my-4">
			<div class="col-12 col-md-8 mx-auto">
				<h1 class="text-center">
					View Asset Unit
				</h1>
			</div>
		</div>

		<div class="row">
			
			<div class="col-12 col-md-4 mx-auto">
				
					{{-- start of unit card --}}
					<div class="card">
							
						<img src="{{ $unit->image}}" alt="" class="card-img-top">
						
						<div class="card-body">
							{{-- start of unit name --}}
							<h5 class="card-title">
								{{ $unit->name}}
							</h5>
								{{-- end of unit name --}}

								{{-- start of unit-code --}}
								<p class="card-text">
									 {{ $unit->unit_code}}
								</p>
								{{-- end of unit_code --}}

								{{-- start of category --}}
								<p class="card-text">
										{{ $unit->category->name}}
								</p>
								{{-- end of category --}}

								{{-- start of unit status --}}
								<p class="card-text">
									_status_
								</p>
								{{-- end of unit status --}}

								{{-- start of request --}}
								
									@include('units.partials.request-form')
								
								{{-- end of request --}}

								{{-- start of view btn --}}
								@if(!isset($view))
									<a href="{{ route('units.show', $unit->id)}}" class="btn btn-sm btn-info">
										View
									</a>
								@endif
								{{-- end of view btn --}}

						</div>
						

						<div class="card-footer">
							{{-- start of edit btn --}}
							@include('units.partials.edit-btn')
							{{-- end of edit btn --}}

							{{-- start of delete btn --}}
							@include('units.partials.delete-form')
							{{-- end of delete btn --}}

						</div>
					</div>
					{{-- end of unit card --}}
			</div>
		</div>
	</div>
@endsection