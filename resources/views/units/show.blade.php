@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8">
				@include('units.partials.header')
			</div>
		</div>
		<hr>

		<div class="row">
			<div class="col-md-4">
				<div class="card" id="unitShowCard">
					<img src="{{ $unit->image}}" alt="" class="card-img-top">
				</div>
			</div>

			<div class="col-md-4">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							Asset code: {{ $unit->unit_code}}
						</h5>
						<p class="card-text">
							Status: 
							<span class="badge badge-{{ $unit->availability_id == 1 ? 'success' : ($unit->availability_id == 2 ? 'warning' : 'danger')}}">
										{{ $unit->availability->name}}	
							</span>
						</p>
						<hr>
						<p class="card-text">
							Category: {{ $unit->category->name}}
						</p>
						<p class="card-text">
							Particulars: {{ $unit->particulars}}
						</p>
						<p class="card-text">
							UOM: {{ $unit->uom}}
						</p>
					</div>
					<div class="card-footer">
						@cannot('isAdmin')
						{{-- start of request --}}
							@include('units.partials.request-form')
						{{-- end of request --}}
						@endcannot

						@can('isAdmin')
						{{-- start of edit btn --}}
							@include('units.partials.edit-btn')
						{{-- end of edit btn --}}

						{{-- start of delete btn --}}
							@include('units.partials.delete-form')
						{{-- end of delete btn --}}
						@endcan
					</div>

				</div>
			</div>
		</div>
		
	</div>
@endsection