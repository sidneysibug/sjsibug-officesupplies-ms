<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Office Supplies MS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md">
            <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Office Supplies MS') }}
            </a>

            <button 
              class="navbar-toggler navbar-dark bg-dark" 
              type="button" 
              data-toggle="collapse" 
              data-target="#navbarToggleExternalContent" 
              aria-controls="navbarToggleExternalContent" 
              aria-expanded="false"  
              aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <ul class="navbar-nav mr-auto">
              <a href="{{ url('/') }}"
                  class="card-header">
                    Office Supplies MS
              </a>
            </ul>

             <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
            
        </nav>

       <main class="container-fluid">
            <div class="row m-0">
                @if(Auth::user())
                <div 
                  class="col-md-2 p-0 m-0 sm-collapse md-show navbar-collapse" 
                  id="navbarToggleExternalContent" 
                   
                >

                    <div class="card">
                        <ul class="list-group">
                            
                              <a 
                                href="{{ route('home')}}" 
                                class="list-group-item justify-content-between align-items-left list-group-item-action
                                {{ Route::CurrentRouteNamed('home') ? "active2" : "" }}
                                ">
                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                  <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                                </svg>
                                Dashboard
                              </a>
                            
                              <a 
                                href="{{ route('categories.index')}}" 
                                class="list-group-item justify-content-between align-items-left list-group-item-action
                                {{ Route::CurrentRouteNamed('categories.index') ? "active2": "" }}
                                ">
                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-tag mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M2 2v4.586l7 7L13.586 9l-7-7H2zM1 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 1 6.586V2z"/>
                                  <path fill-rule="evenodd" d="M4.5 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm0 1a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
                                </svg>
                                Categories
                              </a>
                            
                            
                            <a 
                              href="{{ route('units.index')}}" 
                              class="list-group-item justify-content-between align-items-left list-group-item-action
                              {{ Route::CurrentRouteNamed('units.index') ? "active2" : "" }}
                              ">
                              <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-folder2-open mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M1 3.5A1.5 1.5 0 0 1 2.5 2h2.764c.958 0 1.76.56 2.311 1.184C7.985 3.648 8.48 4 9 4h4.5A1.5 1.5 0 0 1 15 5.5v.64c.57.265.94.876.856 1.546l-.64 5.124A2.5 2.5 0 0 1 12.733 15H3.266a2.5 2.5 0 0 1-2.481-2.19l-.64-5.124A1.5 1.5 0 0 1 1 6.14V3.5zM2 6h12v-.5a.5.5 0 0 0-.5-.5H9c-.964 0-1.71-.629-2.174-1.154C6.374 3.334 5.82 3 5.264 3H2.5a.5.5 0 0 0-.5.5V6zm-.367 1a.5.5 0 0 0-.496.562l.64 5.124A1.5 1.5 0 0 0 3.266 14h9.468a1.5 1.5 0 0 0 1.489-1.314l.64-5.124A.5.5 0 0 0 14.367 7H1.633z"/>
                              </svg>
                                Assets
                            </a>
                            
                            @cannot('isAdmin')
                              <a
                                href="{{ route('requestform.index')}}" 
                                class="list-group-item justify-content-between align-items-left list-group-item-action
                                {{ Route::CurrentRouteNamed('requestform.index') ? "active2" : "" }}

                                ">
                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-file-earmark-text mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M4 1h5v1H4a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V6h1v7a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2z"/>
                                  <path d="M9 4.5V1l5 5h-3.5A1.5 1.5 0 0 1 9 4.5z"/>
                                  <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
                                </svg>
                                 Request Form
                                <span class="badge badge-primary">
                                    {{ Session::has('requestform') ? count(Session::get('requestform')) : 0}}
                                </span>
                              </a>
                            @endcannot
                          
                          @auth

                            {{-- <a 
    
                              href="{{ route('transactions.index')}}" 
                              class="btn list-group-item justify-content-between align-items-left list-group-item-action
                              {{ Route::CurrentRouteNamed('transactions.index') ? "active2" : "" }}
                              " 
                              data-toggle="collapse" 
                              href="#collapseExampleRequests" 
                              role="button" 
                              aria-expanded="false" 
                              aria-controls="collapseExampleRequests">
                                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-archive mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2 5v7.5c0 .864.642 1.5 1.357 1.5h9.286c.715 0 1.357-.636 1.357-1.5V5h1v7.5c0 1.345-1.021 2.5-2.357 2.5H3.357C2.021 15 1 13.845 1 12.5V5h1z"/>
                                <path fill-rule="evenodd" d="M5.5 7.5A.5.5 0 0 1 6 7h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5zM15 2H1v2h14V2zM1 1a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H1z"/>
                              </svg>
                              Requests    
                              </a> --}}
                            <a 
                              href="{{ route('transactions.index')}}" 
                              class="list-group-item justify-content-between align-items-left list-group-item-action
                              {{ Route::CurrentRouteNamed('transactions.index') ? "active2" : "" }}
                              "
                              >
                              <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-archive mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2 5v7.5c0 .864.642 1.5 1.357 1.5h9.286c.715 0 1.357-.636 1.357-1.5V5h1v7.5c0 1.345-1.021 2.5-2.357 2.5H3.357C2.021 15 1 13.845 1 12.5V5h1z"/>
                                <path fill-rule="evenodd" d="M5.5 7.5A.5.5 0 0 1 6 7h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5zM15 2H1v2h14V2zM1 1a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H1z"/>
                              </svg>
                              Requests
                            </a>
                            <?php $statuses = App\Status::all(); ?>
                            @foreach($statuses->all() as $status)
                              
                                <a href="{{ url("status/{$status->id}") }}"
                                 class="list-group-item justify-content-between align-items-left list-group-item-action"
                                >
                                  {{ $status->name}}
                                </a>
                              
                            @endforeach
                            
                          @endauth
                            
                        </ul>
                    </div>
                </div>
                 @endif
                <div class="col-md-10 px-0 px-md-5">
                   @yield('content')  
                </div>
            </div>

        </main>
    </div>
</body>
</html>
