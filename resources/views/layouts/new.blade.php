<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Office Supplies MS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Office Supplies MS') }}
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false"  aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
            </button>

            <ul class="navbar-nav mr-auto">
              <a href="{{ url('/') }}"
                  class="card-header">
                    Office Supplies MS
              </a>
            </ul>

             <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
            
        </nav>
   

       <main class="container-fluid">
            <div class="row m-0">
                @if(Auth::user())
                <div 
                  class="col-md-2 p-0 m-0 show navbar-collapse" 
                  id="navbarToggleExternalContent"
                  style="height: 100vh;" 
                >

                    <div class="card">
                        <ul class="list-group">
                            @can('isAdmin')
                              <a 
                                href="{{ route('home')}}" 
                                class="list-group-item list-group-item-action

                                ">
                                Dashboard
                              </a>
                              <a 
                                href="" 
                                class="list-group-item list-group-item-action
                                ">  
                                Manage Users
                              </a>
                              <a 
                                href="{{ route('categories.index')}}" 
                                class="list-group-item list-group-item-action
                                {{ Route::CurrentRouteNamed('categories.index') ? "active" : "" }}
                                ">
                                Categories
                              </a>
                            @endcan
                            
                            <a href="{{ route('units.index')}}" class="list-group-item list-group-item-action">Asset Units</a>

                            @can('isAdmin')
                              <a href="{{ route('units.create')}}" class="list-group-item list-group-item-action">Add asset unit</a>
                            @endcan  

                            
                              <a href="{{ route('requestform.index')}}" class="list-group-item list-group-item-action">
                                Request Form
                                <span class="badge badge-primary">
                                    {{ Session::has('requestform') ? count(Session::get('requestform')) : 0}}
                                </span>
                              </a>

                            <a href="{{ route('transactions.index')}}" class="list-group-item list-group-item-action">Requests</a>
                            <a href="">Pending</a>
                            <a href="">Completed</a>
                            <a href="">Declined</a>
                            
                        </ul>
                    </div>
                </div>
                 @endif
                <div class="col-md-10 px-0 px-md-5">
                   @yield('content')  
                </div>
            </div>

        </main>
    </div>
</body>
</html>
