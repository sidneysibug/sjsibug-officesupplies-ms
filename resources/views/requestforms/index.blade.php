@extends('layouts.app')

@section('content')
	<div class="container">

		{{-- start of request more btn --}}
		<div class="row">
			<div class="col-12 my-4">
				<a href="{{ route('units.index')}}">
					<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				      <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z"/>
				      <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z"/>
				  </svg>
				  Add Requests
				</a>
				 
			</div>
		</div>
		{{-- end of request more btn --}}

		<div class="row">
			<div class="col-12 col-md-8">
				<h4 class="">
					Request Form
				</h4>
			</div>
		</div>

		@if(!Session::has('requestform'))
    {{-- alert start --}}
        <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>You have no requests!</strong> 
        </div>
    {{-- alert end --}}

    @else

		<div class="row">
			<div class="col-12 col-md-12">

				{{-- start of request form table --}}
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Asset code</th>
							<th scope="col">Category</th>
							<th scope="col">Particulars</th>
							<th scope="col">UOM</th>
							<th scope="col">Current Status</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						{{-- start of unit row --}}
						@foreach($units as $unit)
							<tr>
								<th scope="row">{{ $unit->unit_code}}</th>
								<td>{{ $unit->category->name}}</td>
								<td>{{ $unit->particulars}}</td>
								<td>{{ $unit->uom}}</td>
								<td class="badge badge-{{ $unit->availability_id == 1 ? 'success' : ($unit->availability_id == 2 ? 'warning' : 'danger')}}">{{ $unit->availability->name}}</td>

								<td>
									<form 
										action="{{ route('requestform.destroy', $unit->id) }}"
										method="post">
										@csrf
										@method('DELETE')

										<button class="btn btn-sm btn-outline-danger">Remove</button>
										
									</form>
								</td>
								
							</tr>
						@endforeach
					
						{{-- end of unit row --}}
					</tbody>

					<tfoot>
						<tr>
							<td colspan="3">
								<form 
									action="{{ route('transactions.store')}}"
									method="post"
								>
									@csrf

									<label for="reason">Reason for the request:</label>
									<input 
										type="text" 
										name="reason" 
										id="reason"
										class="w-100" 
										required 
									>
									<small class="text-muted mx-auto mb-2 d-block">
										Example: meeting, event, seminar, etc.
									</small>


									<label for="date_needed">Date needed:</label>
									<input 
										type="date" 
										name="date_needed" 
										id="date_needed"
										class="form-control w-100"
										placeholder="Date needed"
										min="<?= date('Y-m-d', strtotime("+2 days")); ?>" 
										max="<?= date('Y-m-d', strtotime("+30 days")); ?>"
										aria-describedby="date_needed"
										required
									>
									<small class="text-muted mx-auto">
										Date when you need this asset
									</small>
				
									<label for="date_needed" class="d-block">Date needed:</label>
									<input 
										type="date" 
										name="date_return" 
										id="date_return"
										class="form-control w-100"
										placeholder="Date return"
										aria-describedby="date_return"
										min="<?= date('Y-m-d', strtotime("+3 days")); ?>" 
										max="<?= date('Y-m-d', strtotime("+30 days")); ?>"
										required
									>
									<small class="text-muted mx-auto">
										Date when you return the asset
									</small>

									<button class="btn btn-sm btn-success d-block w-100 mt-1">Submit</button>
									@includeWhen(Session::has('message'),'partials.alert')

								</form>
							</td>
						</tr>
					
					</tfoot>
				</table>
			</div>
				{{-- end of request form table --}}

				{{-- start of clear request form --}}
				<div class="row mx-auto">
					<div class="col-12 col-md-4 ">
						<form action="{{ route('requestform.clear')}}" method="post">
							@csrf
							@method('DELETE')
							
							<button class="btn btn-sm btn-outline-danger">Clear Request Form</button>
						</form>
					</div>
				</div>
				{{-- end of clear request form --}}

				
			</div>
		</div>
	 @endif
		
	</div>
@endsection