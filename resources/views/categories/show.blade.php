@extends('layouts.app')

@section('content')
	
	<div class="container">

		<div class="row">
			<div class="mt-4 mb-2 col-12">
					@can('isAdmin')
						@include('categories.partials.create-modal')
					@endcan	
			</div>
				
			<div class="col-12 col-md-4">
				@include('categories.partials.admin')
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-6">
				@include('categories.partials.header')
			</div>
		</div>
		<hr>

		<div class="row">
			@include('categories.partials.card', ['view' => false])
		</div>

		{{-- start of unit display --}}
		{{-- start of units display --}}
		<div class="row mt-3">
			@foreach($units as $unit)
				@if($category->id === $unit->category_id)
				<div class="col-12 col-md-4 mx-auto">
					
						{{-- start of unit card --}}
						<div class="card">
							<a 
								href="{{ route('units.show', $unit->id)}}">
								<img src="{{ $unit->image}}" alt="" class="card-img-top">
							</a>
							<div class="card-body">
								
								{{-- start of unit-code --}}
								<p class="card-text">
									<strong>
									Asset code: {{ strtoupper($unit->unit_code)}}
									</strong>
								</p>
								{{-- end of unit_code --}}

								{{-- start of unit status --}}
								<p class="card-text">
										Status:
										<span class="badge badge-{{ $unit->availability_id == 1 ? 'success' : ($unit->availability_id == 2 ? 'warning' : 'danger')}}">
										{{ $unit->availability->name}}	
										</span>
								</p>
								{{-- end of unit status --}}

								{{-- start of unit category, particulars, and UOM --}}
								<button
									class="btn btn-sm border-none" 
									type="button" 
									data-toggle="collapse" 
									data-target="#collapseExample{{ $unit->unit_code}}" 
									aria-expanded="false" 
									aria-controls="collapseExample{{ $unit->unit_code}}">
								    Details
								 </button>

								 <div class="collapse" id="collapseExample{{ $unit->unit_code}}">
									  <div class="card card-body">
									  	<p class="card-text">
									  		Category: {{ $unit->category->name}}
									  	</p>
									    <p class="card-text"> 
												Particulars: {{ $unit->particulars}}
												<small class="d-block">
												UOM: {{ $unit->uom}}
												</small>
											</p>
									  </div>
									</div>
								{{-- end of unit category, particulars, and UOM --}}


								

								{{-- start of request --}}
								@cannot('isAdmin')
										@include('units.partials.request-form')
								@endcannot
								{{-- end of request --}}

								{{-- start of view btn --}}
								@if(!isset($view))
									<a href="{{ route('units.show', $unit->id)}}" class="btn btn-sm btn-info w-100 mt-1">
											View
										</a>
								@endif
								{{-- end of view btn --}}
								
								@can('isAdmin')
									{{-- start of edit btn --}}
									@include('units.partials.edit-btn')
									{{-- end of edit btn --}}

									{{-- start of delete btn --}}
									@include('units.partials.delete-form')
									{{-- end of delete btn --}}
								@endcan

							</div>

						</div>
						{{-- end of unit card --}}
				</div>
				@endif
			@endforeach
		</div>
		{{-- end of units display --}}
		{{-- end of unit display --}}
			



	</div>




	

@endsection
