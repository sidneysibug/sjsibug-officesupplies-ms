@extends('layouts.app')

@section('content')
	
	<div class="container" id="categoryIndex">
		<div class="row">
			<div class="my-4 col-12">
					@can('isAdmin')
						@include('categories.partials.create-modal')
					@endcan
					
			</div>
			<div class="col-12 col-md-6">
				@include('categories.partials.header')
			</div>
		</div>
		
		<hr>

		{{-- alert-message --}}
		@includeWhen(Session::has('message'),'partials.alert')

		{{-- start of assetcategory --}}
			<div class="row" >
				@foreach($categories as $category)
					@include('categories.partials.card')
				@endforeach
			</div>
		{{-- end of asset category --}}

	</div>

@endsection