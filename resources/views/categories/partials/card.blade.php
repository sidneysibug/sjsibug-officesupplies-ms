
  <div class="col-md-8 border-bottom border-bottom-info">
    <div class="card-body">
      <h5 class="card-title justify-content-left">
        {{ $category->name}}
      </h5>
      <p class="card-text">
        Current available: {{ collect($units)->where('category_id',$category->id)->where('availability_id',1)->count()}}
          /{{ collect($units)->where('category_id',$category->id)->count()}}
      </p>
    </div> 
  </div>

  <div class="col-md-4 border-bottom border-bottom-info">
    <div class="card-body">
      <div class="card-title">
         @if(!isset($view))
            {{-- start of view --}}
            <a 
              href="{{ route('categories.show', $category->id)}}" 
              class="btn btn-success">
                View
            </a>
            {{-- end of view --}}
          @endif 

        @can('isAdmin')
        {{-- start of edit --}}
          @include('categories.partials.edit-modal')
        {{-- end of edit --}}

        {{-- start of delete --}}
          <form action="{{ route('categories.destroy', $category->id) }}" method="post" 
            class="d-inline-block">
            @csrf
            @method('DELETE')

            <button class="btn btn-danger">Delete</button>
          </form>
          {{-- end of delete --}}
        @endcan
      </div>
    </div> 
  </div>




  
