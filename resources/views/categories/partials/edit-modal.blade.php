<button 
  type="button" 
  class="btn btn-warning" 
  data-toggle="modal" 
  data-target="#exampleModalEdit" 
  data-whatever="Edit">
  Edit

</button>


<div class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- start of form --}}
        <form action="{{ route('categories.update', $category->id)}}" method="post" class="mt-2">
          @csrf
          @method('PUT')
          

          <label for="name">Asset category:</label>
          <input 
            type="text" 
            name="name" 
            id="name"
            class="form-control form-control-sm" 
            value="{{ $category->name}}"
          >
          <button class="btn btn-sm btn-warning mt-1">Save changes</button>
        </form>
        {{-- end of form --}}


      </div>
    </div>
  </div>
</div>
