<button 
  type="button" 
  class="btn btn-primary" 
  data-toggle="modal" 
  data-target="#exampleModalAddCat" 
  data-whatever="Add">
  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z"/>
      <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z"/>
  </svg>
  New Category

</button>


<div class="modal fade" id="exampleModalAddCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelAddCat" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabelAddCat">New Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('categories.store') }}" method="post">
          @csrf

          <label for="name">Asset category:</label>
          <input 
            type="text" 
            name="name" 
            id="name" 
            class="form-control form-control-sm"
            required
          >
          <button class="btn btn-sm btn-primary mt-1">Add</button>
        </form>
      </div>
    </div>
  </div>
</div>
