

<h6>
			
	<div class="input-group mb-3">
		<div class="input-group-text">
			<a 
				href="{{ route('categories.index') }}" 
				class="mr-3 d-inline-block
				{{ Route::CurrentRouteNamed('categories.index') ? "active1" : "" }}
				">
					All categories	
			</a>
		</div>
		

		{{-- start of search bar --}}
		  <div class="input-group-prepend ml-3">
		    <label class="input-group-text" 
		    	for="inputGroupSelect01">Search</label>
		  </div>
		  <select 
		  	class="custom-select" 
		  	id="inputGroupSelect01">
		    <option selected>...</option>
		    @foreach($categories as $category)
		    	<a href="{{ url("category/{$category->id}") }}">
		    		<option value="{{ $category->id}}">{{ $category->name}}</option>
		    	</a>
			@endforeach
		  </select>
		 {{-- end of search bar --}}
	</div>
		




</h6>